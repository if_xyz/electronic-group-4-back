package com.electronic.pojo;

public class Menu {
    private Integer id;

    private String name;

    private String route;

    private String icon;

    private Integer orders;


    public Menu() {
    }

    public Menu(Integer id, String name, String route, String icon, Integer orders) {
        this.id = id;
        this.name = name;
        this.route = route;
        this.icon = icon;
        this.orders = orders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

}
