package com.electronic.mapper;

import com.electronic.pojo.Tower;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TowerMapper {

    // 获得所有杆塔
    List<Tower> getAllTower();

    List<Tower> getTowerById(@Param("id") String id, @Param("state") String state);

    // 通过杆塔序号(orders)查询杆塔编号(id)
    List<Tower> selectOrdersFromId(@Param("orders")String orders);

    // 增加杆塔
    int insertTower(Tower tower);

    // 更新
    int updateTower(Tower tower);

    // 更改状态
    int changeState(@Param("id") String id, @Param("state") int state);

    // 删除
    int deleteTower(@Param("id") String id);

}
