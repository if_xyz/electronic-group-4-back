package com.electronic.mapper;
import com.electronic.pojo.Menu;
import com.electronic.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LoginMapper {
    User passwordLogin (User user);
    User smsLogin(String phone);
    List<Menu> selectMenuByRoleId(User user);


}
