package com.electronic.pojo;

import java.util.List;

public class Tower {
    /**
     * 杆塔编号
     */
    private String id;

    private List<String> towerIds;

    /**
     * 杆塔序号
     */
    private String orders;

    /**
     * 杆塔状态:0-未启用，1-已启用
     */
    private Integer state;

    /**
     * 杆塔位置
     */
    private String position;

    /**
     *
     */
    private String lineId;

    private static final long serialVersionUID = 1L;

    /**
     * 杆塔编号
     */
    public String getId() {
        return id;
    }

    /**
     * 杆塔编号
     */
    public void setId(String id) {
        this.id = id;
    }

    public List<String> getTowerIds() {
        return towerIds;
    }

    public void setTowerIds(List<String> towerIds) {
        this.towerIds = towerIds;
    }

    /**
     * 杆塔序号
     */
    public String getOrders() {
        return orders;
    }

    /**
     * 杆塔序号
     */
    public void setOrders(String orders) {
        this.orders = orders;
    }

    /**
     * 杆塔状态:0-未启用，1-已启用
     */
    public Integer getState() {
        return state;
    }

    /**
     * 杆塔状态:0-未启用，1-已启用
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 杆塔位置
     */
    public String getPosition() {
        return position;
    }

    /**
     * 杆塔位置
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     *
     */
    public String getLineId() {
        return lineId;
    }

    /**
     *
     */
    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Tower other = (Tower) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getOrders() == null ? other.getOrders() == null : this.getOrders().equals(other.getOrders()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
                && (this.getPosition() == null ? other.getPosition() == null : this.getPosition().equals(other.getPosition()))
                && (this.getLineId() == null ? other.getLineId() == null : this.getLineId().equals(other.getLineId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrders() == null) ? 0 : getOrders().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getPosition() == null) ? 0 : getPosition().hashCode());
        result = prime * result + ((getLineId() == null) ? 0 : getLineId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("  id=").append(id);
        sb.append(", towerIds=").append(towerIds);
        sb.append(", orders=").append(orders);
        sb.append(", state=").append(state);
        sb.append(", position=").append(position);
        sb.append(", lineId=").append(lineId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

}
