package com.electronic.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableData<T> {
    Map<String, Object> data = new HashMap<>();
    Map<String, Map> map = new HashMap<>();
    private long total;
    private List<T> list;

    public TableData(List<T> list, long count) {
        this.total = count;
        this.list = list;
        data.put("list", list);
        data.put("total", count);
        map.put("data", data);
    }

    public Map<String, Map> getData() {
        return map;
    }


}

