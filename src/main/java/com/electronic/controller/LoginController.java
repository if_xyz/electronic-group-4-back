package com.electronic.controller;

import com.electronic.pojo.User;
import com.electronic.service.LoginService;
import com.electronic.utils.UResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.TreeMap;

@RestController
public class LoginController {

    private User user = new User();


    @Autowired
    private UResponse<Integer> uResponse;

    @Autowired
    private UResponse<Map<String, Object>> MenuResponse;
    @Autowired
    private LoginService loginService;

    @RequestMapping("/login")
    @ResponseBody
    public UResponse<Integer> passwordLogin(String username,String password) {
        user=loginService.passwordLogin(new User(username,password));
        //如果state为null则返回0
        if(user == null){
            this.uResponse.setData(0);
        }
        else this.uResponse.setData(loginService.passwordLogin(new User(username,password)).getSteate());
        return uResponse;
    }
    @RequestMapping("/sms")
    @ResponseBody
    public UResponse<Integer> smsLogin(String phone,String code) {
        user=loginService.passwordLogin(new User(phone,code));
        System.out.println(user.getSteate());
        if(user == null){
            this.uResponse.setData(0);
        }
        else this.uResponse.setData(loginService.smsLogin(phone,code).getSteate());
        return uResponse;
    }

    @RequestMapping("/user/init")
    @ResponseBody
    public UResponse<Map<String, Object>> initMenu() {
        System.out.println(user.getSteate());
        //创建Map result添加loginService.selectMenuByRoleId(user)与user
        Map<String, Object> result = new TreeMap<>();
        result.put("menus",loginService.selectMenuByRoleId(user));
        result.put("user",user);
        this.MenuResponse.setData(result);
        return this.MenuResponse;
    }

    @RequestMapping("/user/logout")
    @ResponseBody
    public UResponse<Integer> Logout() {
        this.uResponse.setData(1);
        return this.uResponse;
    }
}
