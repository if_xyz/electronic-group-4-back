package com.electronic.pojo;

import java.util.Date;
import java.util.List;

public class Line {
    /**
     * 线路编号
     */
    private String id;

    private List<String> lineIds;

    /**
     * 线路名称
     */
    private String name;

    /**
     * 塔基数
     */
    private Integer towerNum;

    /**
     * 起始杆号
     */
    private String startNumber;

    /**
     * 结束杆号
     */
    private String endNumber;

    /**
     * 线路长度
     */
    private Double length;

    /**
     * 回路长度
     */
    private Double backLength;

    /**
     * 电压等级
     */
    private String level;

    /**
     * 投运日期
     */
    private Date operationTime;

    /**
     * 启用状态：0-未启用，1-已启用
     */
    private Integer enableStatus;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * 线路编号
     */
    public String getId() {
        return id;
    }

    /**
     * 线路编号
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 线路名称
     */
    public String getName() {
        return name;
    }

    public List<String> getLineIds() {
        return lineIds;
    }

    public void setLineIds(List<String> lineIds) {
        this.lineIds = lineIds;
    }

    /**
     * 线路名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 塔基数
     */
    public Integer getTowerNum() {
        return towerNum;
    }

    /**
     * 塔基数
     */
    public void setTowerNum(Integer towerNum) {
        this.towerNum = towerNum;
    }

    /**
     * 起始杆号
     */
    public String getStartNumber() {
        return startNumber;
    }

    /**
     * 起始杆号
     */
    public void setStartNumber(String startNumber) {
        this.startNumber = startNumber;
    }

    /**
     * 结束杆号
     */
    public String getEndNumber() {
        return endNumber;
    }

    /**
     * 结束杆号
     */
    public void setEndNumber(String endNumber) {
        this.endNumber = endNumber;
    }

    /**
     * 线路长度
     */
    public Double getLength() {
        return length;
    }

    /**
     * 线路长度
     */

    public void setLength(Double length) {
        this.length = length;
    }

    /**
     * 回路长度
     */
    public Double getBackLength() {
        return backLength;
    }

    /**
     * 回路长度
     */
    public void setBackLength(Double backLength) {
        this.backLength = backLength;
    }

    /**
     * 电压等级
     */
    public String getLevel() {
        return level;
    }

    /**
     * 电压等级
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * 投运日期
     */
    public Date getOperationTime() {
        return operationTime;
    }

    /**
     * 投运日期
     */
    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    /**
     * 启用状态：0-未启用，1-已启用
     */
    public Integer getEnableStatus() {
        return enableStatus;
    }

    /**
     * 启用状态：0-未启用，1-已启用
     */
    public void setEnableStatus(Integer enableStatus) {
        this.enableStatus = enableStatus;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Line other = (Line) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getTowerNum() == null ? other.getTowerNum() == null : this.getTowerNum().equals(other.getTowerNum()))
                && (this.getStartNumber() == null ? other.getStartNumber() == null : this.getStartNumber().equals(other.getStartNumber()))
                && (this.getEndNumber() == null ? other.getEndNumber() == null : this.getEndNumber().equals(other.getEndNumber()))
                && (this.getLength() == null ? other.getLength() == null : this.getLength().equals(other.getLength()))
                && (this.getBackLength() == null ? other.getBackLength() == null : this.getBackLength().equals(other.getBackLength()))
                && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
                && (this.getOperationTime() == null ? other.getOperationTime() == null : this.getOperationTime().equals(other.getOperationTime()))
                && (this.getEnableStatus() == null ? other.getEnableStatus() == null : this.getEnableStatus().equals(other.getEnableStatus()))
                && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getTowerNum() == null) ? 0 : getTowerNum().hashCode());
        result = prime * result + ((getStartNumber() == null) ? 0 : getStartNumber().hashCode());
        result = prime * result + ((getEndNumber() == null) ? 0 : getEndNumber().hashCode());
        result = prime * result + ((getLength() == null) ? 0 : getLength().hashCode());
        result = prime * result + ((getBackLength() == null) ? 0 : getBackLength().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getOperationTime() == null) ? 0 : getOperationTime().hashCode());
        result = prime * result + ((getEnableStatus() == null) ? 0 : getEnableStatus().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", towerNum=").append(towerNum);
        sb.append(", startNumber=").append(startNumber);
        sb.append(", endNumber=").append(endNumber);
        sb.append(", length=").append(length);
        sb.append(", backLength=").append(backLength);
        sb.append(", level=").append(level);
        sb.append(", operationTime=").append(operationTime);
        sb.append(", enableStatus=").append(enableStatus);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
