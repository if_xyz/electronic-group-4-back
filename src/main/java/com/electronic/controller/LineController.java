package com.electronic.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.electronic.utils.TableData;
import com.electronic.pojo.Line;
import com.electronic.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class LineController {
    @Autowired
    private LineService lineService;

    // 查询
    @RequestMapping(value = "/line")
    @ResponseBody
    public Map selectLine(@RequestParam(name = "id", required = false) String id,
                          @RequestParam(name = "name", required = false) String name,
                          @RequestParam(name = "state", required = false) String enableStatus,
                          @RequestParam(name = "currentPage") String currentPage,
                          @RequestParam(name = "displayCount") String displayCount) {
        // 分页
        PageHelper.startPage(Integer.valueOf(currentPage), Integer.valueOf(displayCount));

        // 查询
        System.out.println("==================================================selectLine 查询=================================================");
        System.out.println("id: " + id);
        System.out.println("name: " + name);
        System.out.println("state: " + enableStatus);
        List<Line> list = new ArrayList<>();
        if (!id.equals("") || !name.equals("") || !enableStatus.equals("")) {
            list = lineService.getLineById(id, name, enableStatus);

        } else {
            list = lineService.getAllLines();
        }
        // 封装对象
        PageInfo<Line> pageInfo = new PageInfo<>(list);

        return new TableData<>(pageInfo.getList(), pageInfo.getTotal()).getData();
    }

    // 新增
    @PostMapping(value = "/line")
    public Map addLine(@RequestBody Line line) {
        System.out.println("===================================================addLine 新增===================================================");
        System.out.println(line);
        Map<String, Integer> map = new HashMap<>();
        map.put("data", lineService.addLine(line));
        return map;
    }

    // 更新
    @PutMapping(value = "/line")
    public Map updateLine(@RequestBody Line line) {
        System.out.println("=================================================updateLine 更新==================================================");
        System.out.println(line);
        Map<String, Integer> map = new HashMap<>();
        map.put("data", lineService.updateLine(line));
        return map;
    }

    // 更改状态
    @PutMapping(value = "/lineChange")
    public Map<String, Integer> changeState(@RequestBody Line line) {
        System.out.println("===============================================changeState 更改状态===============================================");
        System.out.println("changeState" + line);
        List<Integer> list = new ArrayList<>();
        for (String id : line.getLineIds()) {
            list.add(lineService.changeState(id, line.getEnableStatus()));
        }
        Map<String, Integer> map = new HashMap<>();
        map.put("data", list.size());
        return map;
    }

    // 删除
    @DeleteMapping(value = "/line/{lineId}")
    public Map deleteLine(@PathVariable("lineId") String lineId) {
        System.out.println("===================================================deleteLine 删除================================================");
        System.out.println("lineId: " + lineId);
        Map<String, Integer> map = new HashMap<>();
        map.put("data", lineService.deleteTower(lineId));
        return map;
    }
}
