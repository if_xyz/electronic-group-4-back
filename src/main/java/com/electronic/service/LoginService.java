package com.electronic.service;

import com.electronic.pojo.Menu;
import com.electronic.pojo.User;

import java.util.List;


public interface LoginService {
    User passwordLogin (User user);
    User smsLogin(String phone,String code);
    List<Menu> selectMenuByRoleId(User user);

}
