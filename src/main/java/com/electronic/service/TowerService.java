package com.electronic.service;

import com.electronic.pojo.Tower;

import java.util.List;

public interface TowerService {

    List<Tower> getAllTower();

    List<Tower> getTowerById(String id, String state);

    List<Tower> selectOrdersFromId(String orders);

    int insertTower(Tower tower);

    int updateTower(Tower tower);

    int changeState(String id, int state);

    int deleteTower(String id);
}
