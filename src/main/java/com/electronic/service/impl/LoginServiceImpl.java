package com.electronic.service.impl;
import com.electronic.mapper.LoginMapper;
import com.electronic.pojo.Menu;
import com.electronic.pojo.User;
import com.electronic.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginMapper loginMapper;
    public User passwordLogin(User user) {
        return loginMapper.passwordLogin(user);
    }
    @Override
    public User smsLogin(String phone,String code) {
        return loginMapper.smsLogin(phone);
    }
    @Override
    public List<Menu> selectMenuByRoleId(User user) {
        return loginMapper.selectMenuByRoleId(user);
    }
}
