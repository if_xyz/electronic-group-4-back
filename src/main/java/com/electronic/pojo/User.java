package com.electronic.pojo;

import javax.xml.crypto.Data;

public class User {
    private String username;
    private String name;
    private String password;
    private String email;
    private String phone;
    private Integer sex;
    private Integer age;
    private Integer steate;
    private Integer role_id;

    private Data entry_time;

    private Data leave_time;

//    private Data create_time;



    public User(String username, String password, String name,
                Integer sex, Integer age, String phone, String email, Integer steate,
                Data entry_time,Data leave_time,Data create_time,Integer role_id){
        this.username = username;
        this.name = name;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.sex = sex;
        this.age = age;
        this.steate = steate;
        this.entry_time = entry_time;
        this.leave_time = leave_time;
//        this.create_time = create_time;
        this.role_id = role_id;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    //全参数构造方法



    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex){
        this.sex=sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSteate() {
        return steate;
    }

    public void setSteate(Integer steate) {
        this.steate = steate;
    }

    public Integer getRole_id() {
        return role_id;
    }

    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }

    public Data getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(Data entry_time) {
        this.entry_time = entry_time;
    }

    public Data getLeave_time() {
        return leave_time;
    }

    public void setLeave_time(Data leave_time) {
        this.leave_time = leave_time;
    }

//    public Data getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Data create_time) {
//        this.create_time = create_time;
//    }


    @Override
    public String toString() {
        return "User{" +
                "username=" + username +
                ", password='" + password + '\'' +
                '}';
    }
}
