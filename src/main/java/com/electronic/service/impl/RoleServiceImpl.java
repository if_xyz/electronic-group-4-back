package com.electronic.service.impl;

import com.electronic.mapper.roleMapper;
import com.electronic.pojo.Role;
import com.electronic.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private roleMapper roleMappers;
    @Override
    public List<Role> selectRole(Role roles) {
        return roleMappers.selectRole(roles);
        // return  null;
    }
    @Override
    public int updateRole(Role role){
        return roleMappers.updateRole(role);
    }
}
