package com.electronic.mapper;

import com.electronic.pojo.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface roleMapper {
    List<Role> selectRole(Role role);
    int updateRole(Role role);
}
