package com.electronic.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.electronic.utils.TableData;
import com.electronic.pojo.Tower;
import com.electronic.service.TowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TowerController {
    @Autowired
    private TowerService towerService;

    // 查询
    @RequestMapping(value = "/tower")
    @ResponseBody
    public Map table(@RequestParam(name = "id", required = false) String id,
                     @RequestParam(name = "state", required = false) String state,
                     @RequestParam(name = "currentPage") String currentPage,
                     @RequestParam(name = "displayCount") String displayCount) {
        // 分页
        PageHelper.startPage(Integer.valueOf(currentPage), Integer.valueOf(displayCount));
        // 查询
        System.out.println("table-id: " + id);
        System.out.println("table-state: " + state);
        List<Tower> list = null;
        if (!id.equals("") || !state.equals("")) {
            list = towerService.getTowerById(id, state);

        } else {
            list = towerService.getAllTower();
        }
        // 分装对象
        PageInfo<Tower> pageInfo = new PageInfo<>(list);
        // resp: {"data":{"list":[{Tower序列化之后的json串}, {}, {}], "total":100}}
        return new TableData<>(pageInfo.getList(), pageInfo.getTotal()).getData();
    }

    @RequestMapping(value = "/towerStart")
    @ResponseBody

    // 查询
    public Map selectLineNumber(@RequestParam(name = "queryString", required = false) String startNumber,
                                @RequestParam(name = "state", required = false) String state,
                                @RequestParam(name = "currentPage") String currentPage,
                                @RequestParam(name = "displayCount") String displayCount) {
        // 分页
        PageHelper.startPage(Integer.valueOf(currentPage), Integer.valueOf(displayCount));
        // 查询
        System.out.println("=================================================selectLineNumber查询============================================");
        System.out.println("selectLineNumber-startNumber: " + startNumber);
        List<Tower> list = null;
        list = towerService.getTowerById(startNumber, state);
        // 分装对象
        PageInfo<Tower> pageInfo = new PageInfo<>(list);
        // resp: {"data":{"list":[{Tower序列化之后的json串}, {}, {}], "total":100}}
        return new TableData<>(pageInfo.getList(), pageInfo.getTotal()).getData();
    }

    // 通过杆塔序号(orders)查询杆塔编号(id)
    @GetMapping(value = "/tower/{orders}")
    public Map selectOrdersFromId(@PathVariable("orders") String orders) {
        System.out.println("============================================通过杆塔序号(orders)查询杆塔编号(id)===================================");
        System.out.println("orders: " + orders);
        Map<String, String> map = new HashMap<>();
        List<Tower> list = towerService.selectOrdersFromId(orders);
        map.put("data", list.get(0).getId());
        return map;
    }

    // 通过杆塔序号(orders)查询杆塔编号(id)
    @GetMapping(value = "/tower/{orders}/{num}")
    public Map fillEndNumber(@PathVariable("orders") String orders, @PathVariable("num") String num) {
        System.out.println("============================================拼接杆塔序号(orders)查询杆塔编号=======================================");
        System.out.println("orders: " + orders);
        String prefix = orders.substring(0, orders.length() - 3);
        System.out.println("prefix: " + prefix);
        String end = Integer.valueOf(orders.substring(orders.length() - 3)) + Integer.valueOf(num) + "";
        while (end.length() < 3) {
            end = "0" + end;
        }
        String endOrders = prefix + end;
        System.out.println("拼接后orders: " + endOrders);
        Map<String, String> map = new HashMap<>();
        map.put("data", endOrders);
        return map;
    }


    // 新增
    @PostMapping(value = "/tower")
    public Map<String, Integer> addTower(@RequestBody Tower tower) {
        System.out.println("===================================================addTower 新增=================================================");
        System.out.println(tower);
        Map<String, Integer> map = new HashMap<>();
        map.put("data", towerService.insertTower(tower));
        return map;
    }

    // 更新
    @PutMapping(value = "/tower")
    public Map<String, Integer> updateTower(@RequestBody Tower tower) {
        System.out.println("=================================================updateTower 更新================================================");
        System.out.println("updateTower： " + tower);
        Map<String, Integer> map = new HashMap<>();
        map.put("data", towerService.updateTower(tower));
        return map;
    }

    // 更改状态
    @PutMapping(value = "/change")
    public Map<String, Integer> changeState(@RequestBody Tower tower) {
//        System.out.println("changeState" + tower);
        List<Integer> list = new ArrayList<>();
        for (String id : tower.getTowerIds()) {
            list.add(towerService.changeState(id, tower.getState()));
        }
        Map<String, Integer> map = new HashMap<>();
        map.put("data", list.size());
        return map;
    }

    // 删除
    @DeleteMapping(value = "/tower")
    public Map<String, Integer> deleteTower(@RequestBody Tower tower) {
//        System.out.println("deleteTower: " + tower);
        List<Integer> list = new ArrayList<>();
        for (String id : tower.getTowerIds()) {
            list.add(towerService.deleteTower(id));
        }
        Map<String, Integer> map = new HashMap<>();
        map.put("data", list.size());
        return map;
    }


}
