package com.electronic.service.impl;

import com.electronic.pojo.Tower;
import com.electronic.mapper.TowerMapper;
import com.electronic.service.TowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TowerServiceImpl implements TowerService {
    @Autowired
    private TowerMapper towerMapper;

    @Override
    public List<Tower> getAllTower() {
        return towerMapper.getAllTower();
    }

    @Override
    public List<Tower> getTowerById(String id, String state) {
        return towerMapper.getTowerById(id, state);
    }

    @Override
    public List<Tower> selectOrdersFromId(String orders) {
        return towerMapper.selectOrdersFromId(orders);
    }

    @Override
    public int insertTower(Tower tower) {
        return towerMapper.insertTower(tower);
    }

    @Override
    public int updateTower(Tower tower) {
        return towerMapper.updateTower(tower);
    }

    @Override
    public int changeState(String id, int state) {
        return towerMapper.changeState(id, state);
    }

    @Override
    public int deleteTower(String id) {
        return towerMapper.deleteTower(id);
    }
}
