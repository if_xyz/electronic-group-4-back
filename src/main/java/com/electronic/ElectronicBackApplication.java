package com.electronic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.electronic.mapper")
public class ElectronicBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElectronicBackApplication.class, args);
    }

}
