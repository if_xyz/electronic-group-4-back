package com.electronic.controller;

import com.electronic.pojo.Role;
import com.electronic.service.RoleService;
import com.electronic.utils.UResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class roleController {
    @Autowired
    private UResponse uResponse;

    @Autowired
    private RoleService roleService;
    @RequestMapping(value="/role",method = RequestMethod.GET)
    public UResponse<List<Role>> test2(String name,Integer status) {
        System.out.println("ll");
        this.uResponse.setData(roleService.selectRole(new Role(name,status)));
        return uResponse;
    }

    @RequestMapping(value = "/role", method = RequestMethod.PUT)
    public UResponse<Integer> test3(@RequestBody Map map){
        List<Integer>idList=(List<Integer>)map.get("roleIds");
        String name=(String)map.get("name");
        Integer enableStatus=(Integer)map.get("enableStatus");
        int idListsize= idList.size();
        if(idListsize>1)
        {
            int k=0,count=0;
            for (int i = 0; i <idListsize ; i++) {
                k=roleService.updateRole(new Role(idList.get(i),null, enableStatus, ""));
                if(k==1){
                    count++;
                }
                System.out.println(idList.get(i));
            }
            this.uResponse.setData(count);
            return uResponse;
        }
        else {
            this.uResponse.setData(roleService.updateRole(new Role(idList.get(0), name, enableStatus, "")));
            return uResponse;
        }
    }





}
