package com.electronic.mapper;

import com.electronic.pojo.Line;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LineMapper {

    // 获得所有线路
    List<Line> getAllLines();

    // 查询特定
    List<Line> getLineById(@Param("id") String id, @Param("name")String name, @Param("enableStatus")String enableStatus);

    // 增加线路
    int addLine(Line line);

    // 更新线路
    int updateLine(Line line);

    // 更新状态
    int changeState(@Param("id") String id, @Param("enableStatus") int enableStatus);

    // 删除
    int deleteLine(@Param("id") String id);
}
