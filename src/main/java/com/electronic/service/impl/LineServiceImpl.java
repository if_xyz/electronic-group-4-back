package com.electronic.service.impl;

import com.electronic.pojo.Line;
import com.electronic.mapper.LineMapper;
import com.electronic.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LineServiceImpl implements LineService {

    @Autowired
    private LineMapper lineMapper;

    @Override
    public List<Line> getAllLines() {
        return lineMapper.getAllLines();
    }

    @Override
    public List<Line> getLineById(String id, String name, String enableStatus) {
        return lineMapper.getLineById(id, name, enableStatus);
    }

    @Override
    public int addLine(Line line) {
        return lineMapper.addLine(line);
    }

    @Override
    public int updateLine(Line line) {
        return lineMapper.updateLine(line);
    }

    @Override
    public int changeState(String id, int enableStatus) {
        return lineMapper.changeState(id, enableStatus);
    }

    @Override
    public int deleteTower(String id) {
        return lineMapper.deleteLine(id);
    }
}
