package com.electronic.service;

import com.electronic.pojo.Line;

import java.util.List;


public interface LineService {
    List<Line> getAllLines();

    List<Line> getLineById(String id, String name, String enableStatus);

    int addLine(Line line);

    int updateLine(Line line);

    int changeState(String id, int enableStatus);

    int deleteTower(String id);

}
