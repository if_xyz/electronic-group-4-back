package com.electronic.pojo;


public class Role {
    private Integer id;
    private String name;
    private Integer enableStatus;
    private String enName;

    public Role() {
    }

    public Role(String name, Integer enableStatus) {
        this.name = name;
        this.enableStatus = enableStatus;
    }

    public Role(Integer roleId, String roleName, Integer enableStatus, String enName) {
        this.id = roleId;
        this.name = roleName;
        this.enableStatus = enableStatus;
        this.enName = enName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Integer enableStatus) {
        this.enableStatus = enableStatus;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + id +
                ", roleName='" + name + '\'' +
                ", enableStatus=" + enableStatus +
                ", enName='" + enName + '\'' +
                '}';
    }
}
