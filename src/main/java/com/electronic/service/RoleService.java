package com.electronic.service;

import com.electronic.pojo.Role;

import java.util.List;

public interface RoleService {
    List<Role> selectRole(Role role);
    int updateRole(Role role);
}
